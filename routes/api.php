<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\FormController;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [LoginController::class, 'login']);


Route::post('/register', [RegisterController::class, 'register']);

Route::group(['middleware' => 'auth:sanctum'], function () {
    Route::get('/login', [LoginController::class, 'getSessionStatus']);
    Route::delete('/login', [LoginController::class, 'logout']);
    Route::get('/link/{fileId}', [FormController::class, 'getLink'])->name('link');
    Route::get('/file/{fileId}', [FormController::class, 'getFileInfo']);
        Route::post('/form', [FormController::class, 'postLink']);
});
