# send-file

## Project setup
```
composer install
```

## generate key for the project
```
php artisan key:generate
```

### run the project
```
php artisan serve
```

### start and run queue
```
php artisan queue:work
```

