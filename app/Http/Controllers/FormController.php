<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Models\SendLinkRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use \App\Jobs\SendLinkEmail;


class FormController extends Controller
{
    public function postLink(Request $request)
    {

        $owner = Auth::user();

        $file = $this->saveFile($request);

        $this->saveSendLinkRequest($request, $file,  $owner);

        $job = (new SendLinkEmail([
            'title' => $request->title,
            'description' => $request->description,
            'owner_name' => $owner->name,
            'recipient_email' => $request->recipient_email,
            'link'  => 'http://localhost:8080/?link=' . $file->id
        ]));
        dispatch($job);

        return response("ok", 200);
    }
    public function getLink(Request $request, $fileId)
    {
        $file = File::find($fileId);

        if (!$file) {
            return response("file not found", 417);
        }

        return Storage::download($file->path, $file->name);
    }

    public function getFileInfo(Request $request, $fileId)
    {
        $file = File::find($fileId);
        if (!$file) {
            return response("file not found", 417);
        }
        return ['file' => $file];
    }

    protected function saveFile(Request $request)
    {
        $fileData = $request->file('file');
        $local_path = 'files/' . random_int(0, 1000) . '/' . $fileData->getClientOriginalName();
        $file = new File;
        $file->name = $fileData->getClientOriginalName();
        $file->extension = pathinfo($local_path, PATHINFO_EXTENSION);;
        Storage::disk('local')->put($local_path, file_get_contents($fileData));
        $file->path = $local_path;
        $file->save();

        return $file;
    }
    protected function saveSendLinkRequest(Request $request, File $file, $owner)
    {
        $sendLinkRequest = new SendLinkRequest;
        $sendLinkRequest->title = $request->title;
        $sendLinkRequest->description =  $request->description;
        $sendLinkRequest->recipient_email = $request->recipient_email;
        $sendLinkRequest->owner_id = $owner->id;
        $sendLinkRequest->file_id =  $file->id;
        $sendLinkRequest->save();
    }
}
