<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $token = null;
        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $token = $user->createToken("my-app-token")->plainTextToken;
             $response = [
            'user' => $user,
            'token' => $token
        ];
            return response($response, 201);
        }
        
        return response()->json(['error' => 'Unauthenticated.' ], 401);

    }

    function getSessionStatus(Request $request)
    {
        $user = Auth::user();
        $response = [
            'user' => $user,
        ];
        return response($response, 201);
    }

    public function logout(Request $request)
    {
        $user = request()->user(); 
        $user->tokens()->where('id', $user->currentAccessToken()->id)->delete();
      return response()->json("ok");
    }

}
