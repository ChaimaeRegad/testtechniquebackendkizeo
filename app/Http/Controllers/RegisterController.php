<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function register(Request $request) {
		$validatedData = $request->validate([
			'name' => 'required|string|max:255',
			'email' => 'required|email|unique:users,email',
			'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'min:6'
		]);

		$validatedData['password'] = Hash::make($validatedData['password']);

        $user = User::create($validatedData);

		if($user) {
            $token = $user->createToken('my-app-token')->plainTextToken;
            $response = [
                'user' => $user,
                'token' => $token
            ];
        
             return response($response, 201);
		}

		return response()->json(null, 404);
	}
}
