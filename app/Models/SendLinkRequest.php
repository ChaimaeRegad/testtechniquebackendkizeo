<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\File;

class SendLinkRequest extends Model
{
    use HasFactory;

         /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'recipient_email'
    ];

    public function owner() {
        return $this->belongsTo(User::class, 'owner_id');
      }

    public function file() {
        return $this->belongsTo(File::class, 'file_id');
      }
}
