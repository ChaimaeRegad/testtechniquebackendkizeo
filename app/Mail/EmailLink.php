<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailLink extends Mailable
{
    use Queueable, SerializesModels;

    protected $emailData;

    public function __construct($emailData)
    {
        // The $notifiable is already a User instance so not really necessary to pass it here
        $this->emailData = $emailData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this
            ->markdown('mail_link')
            ->subject($this->emailData['title'])
            ->with([
                'owner_name' => $this->emailData['owner_name'],
                'description' => $this->emailData['description'],
                'link' => $this->emailData['link']
            ]);
    }
}
